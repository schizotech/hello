# Hello

hey Ns, I'm going to make the SchizoLab "oracle" into a product soon.

I will make the repos public once I start to sell it so I can cash out my grindset before some guy just clones my repos makes it to market first.

# Progress

- [x] Firmware
- [x] Prototype
- [x] Re-layout
- [x] Concept CAD
- [x] Production CAD
- [x] Test mold making
- [ ] Production mold making
- [ ] Proper website front page
- [ ] Online store listing

# Links

[test store](https://wordpress.test.schizolab.com/)

# Recent updates

I've re-layout the board, panelized it, it's being produced. 

![panelization preview](images/panelization-complete.png)

I've also been putting orders to multiple PCB manufacturers, testing which one has the best deliverability, quality, and consistency, knew 1 is trash.

I'm also working with front panel suppliers, been testing out their acrylic front panel with UV printing and custom cut adhesive layer.
